import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import './assets/default.css';

const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674',
    background: '#dfdfdf'
  };

//createApp(App).use(router).mount('#app')
const app = createApp(App)
app.use(router)
app.use(VueSweetalert2, options)
app.mount('#app')