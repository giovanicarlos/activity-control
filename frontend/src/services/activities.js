import { http } from './config'

export default {

    list:(status) => {
        return http.get('atividades/' + status)
    },

    save:(activity) => {
        return http.post('atividade', activity)
    },

    edit:(activity) => {
        return http.put('atividade', activity)
    },

    get:(id) => {
        return http.get('atividade/' + id)
    },

    remove:(id) => {
        return http.delete('atividade/' + id)
    },

    terminate:(id) => {
        return http.put('concluir-atividade/' + id)
    }

}