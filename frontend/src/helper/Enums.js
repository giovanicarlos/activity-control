export function getEnumText(array, value) {
    try {
        let text = array.find(key => key.value === value).text
        return text
    } catch(e) {
        return '';
    }
}
export const EnumActivityType = {
    DESENVOLVIMENTO: 0,
    ATENDIMENTO: 1,
    MANUTENCAO: 2,
    MANUTENCAO_URGENTE: 3
}
export const EnumActivityTypeValues = [
    { value: EnumActivityType.DESENVOLVIMENTO, text: 'Desenvolvimento'},
    { value: EnumActivityType.ATENDIMENTO, text: 'Atendimento'},
    { value: EnumActivityType.MANUTENCAO, text: 'Manutenção'},
    { value: EnumActivityType.MANUTENCAO_URGENTE, text: 'Manutenção Urgente'}
]