import { createWebHistory, createRouter } from "vue-router";
import Home from "@/views/Home.vue";
import Activity from "@/views/Activity.vue";
import ActivityDone from "@/views/ActivityDone.vue";
import Register from "@/views/Register.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/atividades",
    name: "atividades",
    component: Activity,
  },  
  {
    path: "/atividades-concluidas",
    name: "atividades-concluidas",
    component: ActivityDone,
  },  
  {
    path: "/atividade-criar",
    name: "nova-atividade",
    component: Register,
  },  
  {
    path: "/atividade-editar/:id",
    name: "edicao-de-atividade",
    component: Register,
  },  
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;