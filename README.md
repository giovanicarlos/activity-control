# Gerenciador de Tarefas
## _Instruções_

## 1. Banco de dados
Banco de dados PostgreSql.
**Iniciar:** acesse o banco de dados e crie uma base de dados com o nome: activitiesdb

## 2. Backend
API Rest desenvolvida em Java Spring Boot com JPA Hibernate.
**Iniciar:** rodar app ApirestApplication.java

## 3. Frontend
Aplicação Web desenvolvida em Vue.js 3.0, efetuando requisições para os endpoints da API backend.
**Iniciar:** no prompt de comando navegar até a pasta "frontend" do projeto, em seguida:
### Baixar dependências:
```sh
npm install
```

### Iniciar:
```sh
npm run serve
```

## _Observação_
- Se for necessário alterar a URL da API altere o arquivo: \frontend\src\services\config.js
