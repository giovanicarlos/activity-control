package br.com.andrade.apirest.enums;

// Enumerator sobre o status da atividade
public enum StatusActivity {

	ABERTA(0),
	CONCLUIDA(1);
	
	private final Integer val;
	
	private StatusActivity(int v) {
		val = v;
	}
	
	public int getVal() {
		return val;
	}

}
