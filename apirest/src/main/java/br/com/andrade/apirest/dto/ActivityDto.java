package br.com.andrade.apirest.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.andrade.apirest.enums.StatusActivity;
import br.com.andrade.apirest.enums.TypeActivity;

// Data Transfer Object para retornar os dados de uma atividade na API
public class ActivityDto {

	private Integer id;
	
	private String name;
	
	private StatusActivity status;
	
	private TypeActivity type;
	
	private String description;
	
	private Date terminated;
	
	private boolean canFinish;
	
	private boolean canRemove;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status.getVal();
	}

	public void setStatus(StatusActivity status) {
		this.status = status;
	}

	public Integer getType() {
		return type.getVal();
	}

	public void setType(TypeActivity type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	// Já devolvendo a data em formato String formatada pt_BR
	public String getTerminated() {
		if(terminated == null)
			return "";
		else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");		
			return sdf.format(terminated);
		}
	}

	public void setTerminated(Date terminated) {
		this.terminated = terminated;
	}
	
	public boolean getCanFinish() {
		return canFinish;
	}
	
	public void setCanFinish(boolean canFinish) {
		this.canFinish = canFinish;
	}
	
	public boolean getCanRemove() {
		return canRemove;
	}
	
	public void setCanRemove(boolean canRemove) {
		this.canRemove = canRemove;
	}

}
