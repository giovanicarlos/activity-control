package br.com.andrade.apirest.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.andrade.apirest.models.Activity;

// Interface para padronizar os métodos do repositório 
// Será usada através de injeção de dependência nas controllers

@Repository
public interface IActivityRepository {
	
	Activity findById(Integer id);
	List<Activity> findByStatus(Integer status);
	void store(Activity activity);
	void remove(Integer id);
	Activity change(Activity activity);
	void terminate(Integer id);

}
