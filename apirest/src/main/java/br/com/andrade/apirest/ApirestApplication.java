package br.com.andrade.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.andrade.apirest.models.Activity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@SpringBootApplication
public class ApirestApplication {

	// Aplicação inicial para rodar a API
	// Para iniciar: botão direito > Run as > Java Application
	public static void main(String[] args) {
		
		SpringApplication.run(ApirestApplication.class, args);
		
	}

}
