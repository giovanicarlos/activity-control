package br.com.andrade.apirest.enums;

// Enumerator sobre o tipo da atividade
// Poderia ter também uma propriedade "description" do tipo String 
// para guardar um texto explicativo de cada tipo
public enum TypeActivity {
	
	DESENVOLVIMENTO(0),
	ATENDIMENTO(1),
	MANUTENCAO(2),
	MANUTENCAO_URGENTE(3);
	
	private final Integer val;
	
	private TypeActivity(int v) {
		val = v;
	}
	
	public int getVal() {
		return val;
	}
    
}
