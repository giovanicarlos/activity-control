package br.com.andrade.apirest.util;

import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

// Métodos úteis para mapeamento de Model para DTO e vice-versa
public class MapperUtil {
	
	private static ModelMapper modelMapper = new ModelMapper();


    private MapperUtil() {


    }

    public static <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {

        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
