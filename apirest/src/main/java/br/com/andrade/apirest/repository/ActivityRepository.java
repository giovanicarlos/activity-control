package br.com.andrade.apirest.repository;

import java.util.List;
import java.util.*;

import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.andrade.apirest.enums.StatusActivity;
import br.com.andrade.apirest.interfaces.IActivityRepository;
import br.com.andrade.apirest.models.Activity;

// Repositório de atividades

@Repository
public class ActivityRepository implements IActivityRepository {
	
	private EntityManager entityManager;
	
	public ActivityRepository() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnitName");
        this.entityManager = entityManagerFactory.createEntityManager();
	}

	// Foi usado JPQL por ser bem simples, mas poderia usar também CRITERIA
	public Activity findById(Integer id) 
	{
        String jpql = "select a from Activity a where a.id = " + id;
        TypedQuery<Activity> typedQuery = this.entityManager.createQuery(jpql, Activity.class);
        Activity activity = typedQuery.getSingleResult();
        return activity;
	}

	public List<Activity> findByStatus(Integer status) 
	{
        String query = "select a from Activity a where a.status = " + status;
        TypedQuery<Activity> typedQuery = this.entityManager.createQuery(query, Activity.class);
        List<Activity> lista = typedQuery.getResultList();
        return lista;		
	}
	
	public void store(Activity activity)
	{
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(activity);
		this.entityManager.getTransaction().commit();
	}
	
	public void remove(Integer id)
	{
		Activity activity = this.entityManager.find(Activity.class, id);
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(activity);
		this.entityManager.getTransaction().commit();		
	}
	
	public Activity change(Activity activity)
	{
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(activity);
		this.entityManager.getTransaction().commit();
		return activity;
	}
	
	public void terminate(Integer id)
	{
		Activity activity = this.entityManager.find(Activity.class, id);
		activity.setStatus(StatusActivity.CONCLUIDA);
		activity.setTerminated(new Date());
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(activity);
		this.entityManager.getTransaction().commit();		
	}
}
