package br.com.andrade.apirest.dto;

//Data Transfer Object para retornar uma resposta padrão dos endpoints da API
public class ResponseApiDto {
	
	// Informa se a operação deu certo
	private boolean success;
	
	// Informa a mensagem de erro caso aconteça
	private String error;
	
	public ResponseApiDto(boolean success, String error) {
		this.success = success;
		this.error = error;
	}
	
	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
