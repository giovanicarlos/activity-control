package br.com.andrade.apirest.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.andrade.apirest.enums.StatusActivity;
import br.com.andrade.apirest.enums.TypeActivity;

// Classe que abstrai a tabela TB_ACTIVITY

@Entity
@Table(name="TB_ACTIVITY")
public class Activity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="activity_sequence")
    @SequenceGenerator(name="activity_sequence", sequenceName="act_seq")
	private Integer id;
	
	private String name;
	
	private StatusActivity status;
	
	private TypeActivity type;
	
	private String description;
	
	private Date terminated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StatusActivity getStatus() {
		return status;
	}

	public void setStatus(StatusActivity status) {
		this.status = status;
	}

	public TypeActivity getType() {
		return type;
	}

	public void setType(TypeActivity type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTerminated() {
		return terminated;
	}

	public void setTerminated(Date terminated) {
		this.terminated = terminated;
	}
	
	/* Regras de Domínio */
	
	public boolean getCanRemove() {
		if(type == TypeActivity.MANUTENCAO_URGENTE)
			return false;
		return true;
	}
	
	public boolean getCanFinish() {
		Integer minimum = 50;
		Integer size = description.length();
		if(size < minimum && (type == TypeActivity.ATENDIMENTO || type == TypeActivity.MANUTENCAO_URGENTE))
			return false;
		return true;
	}
	
}
