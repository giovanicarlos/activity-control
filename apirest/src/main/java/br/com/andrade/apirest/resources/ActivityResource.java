package br.com.andrade.apirest.resources;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONPObject;

import br.com.andrade.apirest.dto.ActivityDto;
import br.com.andrade.apirest.dto.ResponseApiDto;
import br.com.andrade.apirest.enums.TypeActivity;
import br.com.andrade.apirest.interfaces.IActivityRepository;
import br.com.andrade.apirest.models.Activity;
import br.com.andrade.apirest.util.MapperUtil;

// Resource (controller) que implementa os endpoints da API referente a Atividades

// Foi adicionado a configuração de CORS, mas o ideal seria autenticar usando um token bearer por exemplo
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/api")
public class ActivityResource {
	
	private static ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	IActivityRepository activityRepository;

	@GetMapping("/atividades/{status}")
	public List<ActivityDto> listActivities(@PathVariable(value="status") Integer status) {
		List<Activity> activities = activityRepository.findByStatus(status);
		List<ActivityDto> activityDtoList = MapperUtil.mapList(activities, ActivityDto.class);
		return activityDtoList;
	}

	@GetMapping("/atividade/{id}")
	public ActivityDto listActivity(@PathVariable(value="id") Integer id) {
		Activity activity = activityRepository.findById(id);
		ActivityDto activityDto = modelMapper.map(activity, ActivityDto.class);
		return activityDto;
	}
	
	@PostMapping("/atividade")
	public ResponseApiDto storeActivity(@RequestBody Activity activity) {
		if(this.canStore(activity)) {
			activityRepository.store(activity);
			return new ResponseApiDto(true, "");
		}
		else {
			return new ResponseApiDto(false, "Não foi possível gerar esse tipo de atividade após esse horário");
		}
	}
	
	@DeleteMapping("/atividade/{id}")
	public void deleteActivity(@PathVariable(value="id") Integer id) {
		activityRepository.remove(id);
	}
	
	@PutMapping("/atividade")
	public ResponseApiDto changeActivity(@RequestBody Activity activity) {
		if(this.canStore(activity)) {
			activityRepository.change(activity);
			return new ResponseApiDto(true, "");
		}
		else {
			return new ResponseApiDto(false, "Não foi possível alterar esse tipo de atividade após esse horário");
		}
	}
	
	@PutMapping("/concluir-atividade/{id}")
	public void terminateActvity(@PathVariable(value="id") Integer id) {
		activityRepository.terminate(id);
	}

	// Por ser apenas uma regra ficou na controller mesmo, 
	// mas o ideal seria ter uma camada 'Service' 
	private boolean canStore(Activity activity) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int day = cal.get(Calendar.DAY_OF_WEEK);
		int hour = cal.get(Calendar.HOUR_OF_DAY);

		// TODO Para ficar configurável podedira gravar no banco de dados esse horário da sexta-feira
		// Não permite gravar a partir de sexta-feira até domingo
		if(activity.getType() == TypeActivity.MANUTENCAO_URGENTE && (day == 6 && hour >= 13 || day == 7 || day == 1))
			return false;
		return true;
	}

}
